---
title: Tradingview开发
categories: Tradingview
tags: Tradingview
date: 2021-11-17 11:00:00
---

## 简介

看到本文默认已经阅读过 Tradingview 文档，并且有一定能力进行开发。

## UDF 模式至少要提供一下四个接口

```javaScript

// 接口一   “/config”  图表配置信息
response = {
    supports_search: true,                                                     // true为支持商品查询
    supports_group_request: false,                                             // false支持商品搜索或单个商品解析
    supported_resolutions: ["1", "5", "15", "30", "60", "240", "480", "1D"],   // K线时间刻度
    supports_marks: false,                                                     // false为不支持标识
    supports_time: true,                                                       // true为提供服务器时间
    supports_timescale_marks: false                                            // false为不支持时间刻度标记
}

// 接口二   “/symbols”  商品信息
response = {
    name: "btc_usdt",                                                         // 商品名称
    ticker: "btc_usdt",                                                       // 商品唯一标识
    description: '',                                                          // 商品说明
    type: 'bitcoin',                                                          // bitcoin为区块链交易的类型
    session: '24x7',                                                          // 商品交易时间
    timezone: 'Asia/Shanghai',                                                // 商品交易所时区
    minmov: 1,                                                                // 最小波动
    minmove2: 0,                                                              // ???
    pricecale: 1,                                                             // 价格精度
    has_seconds: false,                                                       // false为不具有以秒为单位的历史数据
    has_intraday: true,                                                       // true为具有以分钟为单位的历史数据
    supported_resolutions: ["1", "5", "15", "30", "60", "240", "480", "1D"],  // 商品的周期选择器中启用一个周期数组
    has_no_volume: true,                                                      // true拥有成交量数据
    has_fractional_volume: true,                                              // true为成交量指标值将不会舍入为整数值
    volume_precision: 0,                                                      // 商品的成交量数字的小数位，0表示只显示整数
    expired: false,                                                           // false表示商品是没有到期的期货合约
    has_empty_bars: false,                                                    // false为当datafeed没有数据返回时,library不会生成空的K柱
    has_weekly_and_monthly: false,                                            // false为商品不具有以W和M为单位的历史数据
    has_daily: true,                                                          // true为商品具有以日为单位的历史数据
}

// 接口三   “/history”  K线图数据
response = {
    s: "ok",                                              // ok为有数据
    t: [1386493512, 1386493572, 1386493632, 1386493692],  // K线时间戳
    c: [42.1, 43.4, 44.3, 42.8],                          // 收盘价
    o: [41.0, 42.9, 43.7, 44.5],                          // 开盘价
    h: [43.0, 44.1, 44.8, 44.5],                          // 最高价
    l: [40.4, 42.1, 42.8, 42.3],                          // 最低价
    v: [12000, 18500, 24000, 45000]                       // 成交量
}
response = {
    s: "no_data",                                         // no_data为没有数据
    nextTime: 1386493512                                  // 请求期间无数据，nextTime为下一个K线柱的时间
}

// 接口四   “/time”  服务器时间
1537518914    //时间戳，以秒为单位
```
