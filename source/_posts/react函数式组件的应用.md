---
title: react函数式组件的应用
categories: React
tags: React
date: 2021-02-23 17:20:00
---

## 简介

react 的函数组件也被称为无状态组件，因为函数组件无法拥有自己的状态，保存相关状态，只能根据 props 直接渲染。虽然结构简单明了，但是做不到像类组件一样 有 state 和 setState，有自己的生命周期。

前面谈到 react hooks 的时候 我们知道，可以通过 react hooks 来让函数组件能够拥有自己的状态，实现和类组件一样的功能，更重要的是，函数组件可以抽离出公共部分，在组件的逻辑复用方面要比类组件更为方便。

其实函数组件利用 react hooks 才是真正的逻辑都是由状态驱动，或者说由数据驱动的，这种特性将变得更加纯粹。类组件生命周期的设计模式，更偏向传统的事件驱动模型。

具体 react hooks 用法等，可参考：http://xiuber.gitee.io/blog/2021/01/20/qian-tan-react-hook/

## 多状态 多逻辑

本文依旧根据 react hook 和函数组件，类组件之间的比较，来深入理解函数组件在实际应用中的用法。

我们来看个实例，多状态，多逻辑情况下，类组件做法：

```javascript
class Com extends React.Component {
  state = {
    a: 1,
    b: 2,
    c: 3,
  };

  componentDidMount() {
    handleA();
    handleB();
    handleC();
  }
}
```

如果我们换成函数组件，就可以把状态跟逻辑结合到一起，拆分成多个自定义 hooks，这样代码结构就会变得非常清晰：

```javascript
function useA() {
  const [a, setA] = useState(1);
  useEffect(() => {
    handleA();
  }, []);

  return a;
}

function useB() {
  const [b, setB] = useState(2);
  useEffect(() => {
    handleB();
  }, []);

  return b;
}

function useC() {
  const [c, setC] = useState(3);
  useEffect(() => {
    handleC();
  }, []);

  return c;
}

function Com() {
  const a = useA();
  const b = useB();
  const c = useC();
}
```

## 处理 porps 更新

在以往的类组件中，我们最常用到的生命周期 componentDidUpdatecomponentWillUnmount，其中 props 的更新，和何时需要组件销毁的场景，其实操作起来都会存在各式各样的问题。我们来举例看看 处理 porps 更新，在类组件中如何操作：

```javascript
class Demo extends Component {
  state = {
    data: []
  }
  fetchData = (id, authorId) => {
    // 请求接口
  }
  componentDidMount() {
    this.fetchData(this.props.id, this.props.authorId)
    // ...其它
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.id !== prevProps.id ||
      this.props.authorId !== prevProps.authorId // 别漏了！
    ) {
      this.fetchData(this.props.id, this.props.authorId)
    }

    // ...其它
  }
  render() {
    // ...
  }
}
}
```

我们可以看到，在两个生命周期里，执行的几乎是相同的代码逻辑，在判断是否需要更新的逻辑中我们也经常会遗忘该判断。如果我们使用 react hooks 来实现呢：

```javascript
function Demo({ id, authorId }) {
  const [data, SetData] = useState([]);
  const fetchData = (id, authorId) => {};
  useEffect(() => {
    fetchData(id, authorId);
  }, [id, authorId]);
}
```

代码就这么简单，我们把逻辑状态放入 react hook 的 useEffect 中，只有当 useEffect 中的任意一个参数发生了改变，Effect 才会重新执行。而且每次重新渲染都会生成新的 effect，替换掉之前的，确保 effect 中获取的值是最新的。React 会在执行当前 effect 之前对上一个 effect 进行清除。这样就不用操心什么时候改变状态，什么时候关注组件卸载，比起类组件繁琐的生命周期的事件驱动，数据状态驱动提现的更加明显。

## 优化渲染性能

我们来说下 componentShouldUpdate 钩子函数，我们都知道该函数是为了判断当前是否需要重新渲染。改用 react hook 的函数组件中，我们可以使用 useMemo 来实现该功能。例如：

```javascript
function Demo(props) => {
  useEffect(() => {
    fetchData(props.id)
  }, [props.id])

  return useMemo(() => (
    // ...
  ), [props.id])
}

```

无论 props 如何改变，只要 props.id 不改变，那么该组件就不会进行二次渲染，这样可以进行代码优化，避免无用的渲染，提升代码性能。
