---
title: 浅谈 React Hook
categories: React
tags: React
date: 2021-01-20 17:20:00
---

## 简介

react hook 是 react 16.7 版本后的新特性，主要目的是解决 react 状态共享问题。如果是以前，为了对状态进行管理，我们经常的做法是使用类组件或者使用 redux 等状态管理框架。例如：

```javascript
import React, { Component } from "react";

class Hooks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  render() {
    return (
      <div>
        <p>You clicked {this.state.count} times</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}

export default Hooks;
```

16.7 版本以后呢，我们可以使用 react hooks 提供的 State Hook 来处理状态，针对已经存在的类组件，也可以使用 hooks 进行重构。例如：

```javascript
import React, { useState } from "react";

function Hooks() {
  const [count, setCount] = useState(0);
  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}
```

经过我们的修改，我们可以看到，变成了一个函数组件，该函数组件有着自己的状态，也可以更新自己的状态，这正是因为有了 useState 函数，useState 是 hook 自带函数，用来声明状态变量。

## Hook API

在实际的开发中，我们知道如果要复用一个带有业务逻辑的组件是很困难繁琐的，react 提供了两种创建组件的方式，函数组件和类组件。

函数组件是一个 js 函数，接收 porps 对象，返回 react 元素。但是函数组件缺乏类组件的状态，生命周期等特性。react hook 的出现，让函数组件也可以拥有类组件的特性。

react 提供了三个核心 API，State Hooks、Effect Hooks 和 Custom Hooks。useState 是其中最基础，最常用的 hook，主要用来定义管理组件本地状态。实例如下：

```javascript
import React, { useState } from "react";

function getNumber() {
  const [count, setCount] = useState(0);
  return (
    <div>
      <button onClick={() => setCount(count + 1)}>+</button>
      <span>{count}</span>
      <button onClick={() => setCount((count) => count - 1)}>-</button>
    </div>
  );
}

export default getNumber;
```

实例中，useState 来定义一个状态，与类组件不同，函数组件的状态可以是对象，可以是其他基础类型。useState 返回一个数组，数组的第一个值，代表当前状态的值，第二个值表示用于改变状态的函数。如果一个函数组件中，存在多个状态，可以通过一个 useState 声明对象类型来管理状态，也可以声明多个 useState 来管理状态。例如：

```javascript
//声明对象类型状态
const [count, setCount] = useState({
  count1: 0,
  count2: 0,
});

//多次声明
const [count1, setCount1] = useState(0);
const [count2, setCount2] = useState(0);
```

但是如果要是处理多层嵌套数据时候，我们可以使用 useReducer。例如：

```javascript
import React, { useReducer } from "react";

const reducer = function (state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    default:
      return { count: state.count };
  }
};

function MoreState() {
  const [state, dispatch] = useReducer(reducer, { count: 0 });
  const { count } = state;
  return (
    <div>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
      <span>{count}</span>
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
    </div>
  );
}

export default MoreState;
```

useReducer 接收两个参数，第一个是处理的 reducer 函数，第二个是默认值。返回值是 state 当前状态和 dispatch 函数的一个数组。基本和 redux 一样，不同的是，redux 的默认值是通过 reducer 函数设置默认参数。useReducer 之所以没有像 redux 一样，是因为 react 认为状态的默认值可能来函数组件的 props。

```javascript
function MoreState({ count: 0 }) {
  const [state, dispatch] = useReducer(reducer, { count: count });
  const { count } = state;
  return (
    <div>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
      <span>{count}</span>
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
    </div>
  );
}
```

解决了函数组件的状态问题，还有生命周期问题。我们看如下实例：

```javascript
import React, { useState, useEffect } from "react";

function MoreState() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log("componentDidMount...");
    console.log("componentDidUpdate...");
    return () => {
      console.log("componentWillUnmount...");
    };
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}

export default MoreState;
```

运行结果如下：
[![useEffect](https://s3.ax1x.com/2021/02/20/y4xYIP.png)](https://imgchr.com/i/y4xYIP)

通过结果，可以出来，每次执行组件更新的时候，useEffect 中的回调函数都被调用了，并且在执行更新的时候，调用了 componentWillUnmount 生命周期。之所以重新绘制页面前进行销毁，是为了避免内存泄漏。所以，我们可以把 useEffect 看做以上三个生命周期的集合。

## 自定义 Hook

自定义 Hook 其实就是个内部使用了 useState 、useEffect 的普通函数，其名称以 “use” 开头，可以将组件逻辑提取到可重用的函数中。我们回到最开始的实例当中,做个改动：

```javascript
import React, { useState, useEffect } from "react";

function getNumber() {
  const [count, setCount] = useState(0);
  let interval = null;

  useEffect(() => {
    interval = setInterval(() => {
      setCount(count + 1);
    }, 1000);

    return clearInterval(interval);
  });
  return (
    <div>
      <span>{count}</span>
    </div>
  );
}

export default getNumber;
```

有没有想过，这个组件如果拿去复用，但是需要的是每次+2 我们应该怎么做？如果重新写个组件，修改的地方无非就是 count+2 这一个小地方。这时候我们可以自定义 Hook，来将其中重复的地方抽离出来。

```javascript
import React, { useState, useEffect } from "react";

function useSetCount(c, n) {
  const [count, setCount] = useState(c);
  useEffect(() => {
    interval = setInterval(() => {
      setCount(count + n);
    }, 1000);
    return clearInterval(interval);
  });

  return count;
}

// 调用方式 ，在A组件中

function A(props) {
  const count = useSetCount(1, 1);
  return (
    <div>
      <span>{count}</span>
    </div>
  );
}

// 在b组件中
function A(props) {
  const count = useSetCount(2, 2);
  return (
    <div>
      <span>{count}</span>
    </div>
  );
}
```

自定义 hook 以 use 开头，可以自由决定它的参数是什么，它可以返回什么。注意：自定义 hook 必须以 use 开头。

## 规范

1.  只在最顶层使用 Hook，不要在循环，条件或嵌套函数中调用 Hook， 确保总是在你的 React 函数的最顶层调用他们，在每一次渲染中都按照同样的顺序被调用。这让 React 能够在多次的 useState 和 useEffect 调用之间保持 hook 状态的正确。
2.  只在 React 函数中调用 Hook 不要在普通的 JavaScript 函数中调用 Hook，遵循此规则，确保组件的状态逻辑在代码中清晰可见。
